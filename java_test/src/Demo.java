import com.pad.Tools;
import java.math.BigDecimal;  

public class Demo {
    /**
     * 定义变量 x, y
     */
    public int x = 10;
    public int y = 30;
    
    public Demo(){
        this(300, 600);
    }
    
    public Demo(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    /**
     * 求和
     * @param agrs 
     * @return int 
     */
    public int sum(){
        int sum = 0;
        sum = this.x + this.y;
        return sum;
    }
    
    public static void main(String[] agrs){
        boolean a = false;
        a = !a;
        
        System.out.println("aaa="+a);
    }
}