/**
 * 线程实例
 * @author Administrator
 */
public class ThreadDemo {
    public static void main(String[] args){
        // 获取当前线程
        Thread t = Thread.currentThread();
        
        // 打印当前线程
        System.out.println("当前线程：" + t);
        
        // 设置线程名字
        t.setName("MyThread");
        
        // 打印设置后台的线程
        System.out.println("修改后的线程：" + t);
        
        // 获取线程名字
        String Thread_name = t.getName();
        
        // 打印线程名字
        System.out.println("线程名字：" + Thread_name);
        
        try{
            for(int i=0; i<5; i++){
                // 输出线程运行调试
                System.out.println("i=" + i);

                // 线程挂起 3 秒
                Thread.sleep(3000);
            }
        }catch(InterruptedException e){
            System.out.println("中断异常：" + e);
        }
    }
}