/**
 * 工具集
 */
package com.pad;

public class Tools{
    /**
     * 名字 
     * @var String name
     */
    public String name = "小李";
    
    /**
     * 年龄
     * @var int age
     */
    public int age = 30;
    
    /**
     * 获取名字
     * @return String 
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * 设置名字
     * @param String name   名字
     * @return void
     */
    public void setName(String name){
        this.name = name;
    }
    
    /**
     * 设置年龄
     * @param int age   年龄
     * @return void
     */
    public void setAge(int age){
        this.age = age;
    }
    
    /**
     * 获取年龄
     * @return 
     */
    public int getAge(){
        return this.age;
    }
}

class Dog{
    public String name = "小狗";
}