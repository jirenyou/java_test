/**
 * 测试 String 和 StringBuffer 执行效率比较
 * @author jirenyou
 */
public class StringTest {
    public static void main(String[] args){
        //运算次数
        int count = 10000;
        //运算字符串
        String str = "abcdefghijklmnopqrstuvwxyz";
        
        //测试String运行时间
        String str1 = "";
        //运行开始时间
        long string_time_start = System.currentTimeMillis();
        for(int i=0; i<count; i++){
            str1 += str;
        }
        //结束时间
        long string_time_end = System.currentTimeMillis();
        
        //输出
        System.out.println("String运行时间：" + (string_time_end - string_time_start) + "ms");
        
        //测试StingBuffer运行时间
        StringBuffer str2 = new StringBuffer();
        //运行开始时间
        long string_buffer_time_start = System.currentTimeMillis();
        for(int j=0; j<count; j++){
            str2.append(str);
        }
        //运行结束时间
        long string_buffer_time_end = System.currentTimeMillis();
        
        //输出
        System.out.println("StringBuffer运行时间："+(string_buffer_time_end-string_buffer_time_start)+"ms");
    }
}