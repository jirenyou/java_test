/**
 * 多线程
 */
public class NewThread {
    public static void main(String[] args){
        // 主线程开始
        System.out.println("主线程开始...");
        
        // 获取当前线程
        Thread c_t = Thread.currentThread();
        
        // 设置当前线程名称
        c_t.setName("Main_Thread");
        
        // 打印当前线程
        System.out.println("主线程：" + c_t);
        
        // 创建一个新线程（异步：主线程、新线程同时执行）
        MyThread T1 = new MyThread("one");
        MyThread T2 = new MyThread("two");
        MyThread T3 = new MyThread("three");
        MyThread T4 = new MyThread("four");
        
        // 捕获线程中断
        try{
            for(int a=5; a>0; a--){
                // 输出主进程运行信息
                System.out.println("a=" + a);
                
                // 线程挂起 2 秒
                Thread.sleep(2000);
            }
            
            // 等待所有线程执行完毕
            T1.t.join();
            T2.t.join();
            T3.t.join();
            T4.t.join();
            
        }catch(InterruptedException e){
            System.out.println("线程中断：" + e);
        }
        
        // 创建一个新线程（同步：主线程执行完毕才执行）
        //new MyThread();
        
        // 主线程退出
        System.out.println("主线程退出！");
    }
}

/**
 * 线程类
 * @author Administrator
 */
class MyThread implements Runnable {
    // 线程变量
    Thread t;
    
    // 线程名字
    String name;
    
    // 构造函数
    MyThread(String thread_name){
        name = thread_name;
        
        // 线程开始
        System.out.println("新线程开始...");
        
        // 创建一个新线程
        t = new Thread(this, name);
        
        // 打印线程
        System.out.println("新线程：" + t);
        
        // 启动线程
        t.start();
    }
    
    // 运行线程
    public void run(){
        // 线程开始运行
        System.out.println("新线程开始运行...");
        
        // 捕获中断异常
        try{
            for(int i=0; i<5; i++){
                // 输出信息
                System.out.println("i=" + i);
                // 线程挂起2秒
                Thread.sleep(2000);
            }
        }catch(InterruptedException e){
            System.out.print("终端异常：" + e);
        }
        
        
        // 退出线程
        System.out.println("新线程已退出！");
    }
}